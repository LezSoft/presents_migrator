/*
 * This file is part of presents_migrator.
 *   presents_migrator is free software developed by LezSoft. While LezSoft holds all rights on the presents_migrator brand and logo,
 *   you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 *   Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 *   SPDX-FileCopyrightText:  © 2020-2022 Lezsoft <https://lezsoft.com>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:presents_migrator/services/fireabse_auth.dart';
import 'package:presents_migrator/services/migrator.dart';
import 'package:presents_migrator/utils/default_text_form_field.dart';
import 'package:presents_migrator/utils/show_reset_password_dialog.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _email = TextEditingController();
  final TextEditingController _psw = TextEditingController();

  String _googleMessage = '';
  String _errorMessage = '';
  String _successMessage= '';

  final FirebaseAuthService _firebaseAuth = FirebaseAuthService();
  final Migrator _migrator = Migrator();

  late final StreamSubscription _progressStreamSub;
  double _migrationProgress = 0;

  @override
  void initState() {

    _progressStreamSub = _migrator.migrationProgress.listen(
      (progress) {
        setState(() => _migrationProgress = progress);
      },
      onError: (error) {
        if (!error.contains("google") ) {
          setState(() {
            _errorMessage = error;
            _migrationProgress = 0;
          });
        }
        else {
          setState(() => _googleMessage = error);
        }
      },
      onDone: () {
        setState(() {
          _successMessage =
            "SUCCESS! Your accunt has been successfully migrated to Presents V2.\n"
            "You can close this page and enjoy your new Presents experience";
          _errorMessage = "";
        });
      }
    );

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Presents Migrator"),
      ),
      body: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          // padding
          Flexible(
            flex: 1,
            child: Container()
          ),
          // Actual content
          Flexible(
            flex: 5,
            child: SingleChildScrollView(
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                color: Colors.blueGrey[100],
                child: Padding(
                  padding: const EdgeInsets.all(25.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      //region HEADING
                      const Text(
                        "Welcome to the Presents Migrator!",
                        textAlign: TextAlign.center,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      const SizedBox(height: 10.0,),
                      const Text(
                        "This tool will let you migrate your old PresentsV1 account to the new version of Presents",
                        style: TextStyle(fontStyle: FontStyle.italic),
                        textAlign: TextAlign.center,
                      ),
                      const SizedBox(height: 10.0,),
                      RichText(
                        text: const TextSpan(
                          children: [
                            TextSpan(
                              text: "To proceed, log in with your PresentsV1 credentials and tap the ",
                              style: TextStyle(color: Colors.black)
                            ),
                            TextSpan(
                              text: "MIGRATE",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontStyle: FontStyle.italic,
                                color: Colors.black
                              )
                            ),
                            TextSpan(text: " button", style: TextStyle(color: Colors.black)),
                          ]
                        ),
                        textAlign: TextAlign.center,
                      ),
                      const SizedBox(height: 25.0,),
                      //endregion

                      //region LOG IN SECTION

                      //Google LogIn Button
                      ElevatedButton.icon(
                        onPressed: () async {
                          String? v1uid = await _firebaseAuth.googleLogIn();
                          if (v1uid == 'process-aborted') {
                            setState(() => _errorMessage = "ERROR: something went wrong while loggin in with Google,\n please try again.");
                          }
                          else {
                            _migrator.migrateUser(v1uid, null, fromGoogleLogin: true);
                          }
                        },
                        style: ElevatedButton.styleFrom(
                          padding: const EdgeInsets.all(15.0),
                        ),
                        label: const Text("Log in with Google"),
                        icon: const Icon(MdiIcons.google),
                      ),
                      const SizedBox(height: 25.0,),

                      if (_googleMessage != '')
                        Text(
                          _googleMessage,
                          style: TextStyle(
                            color: Colors.green[900],
                            fontStyle: FontStyle.italic,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      if(_googleMessage != '')
                        const SizedBox(height: 25.0,),

                      Form(
                        key: _formKey,
                        child: Column(
                          children: [
                            DefaultTextFormField(
                              controller: _email,
                              icon: Icons.alternate_email,
                              label: "Email",
                              validator: (value) {
                                if (value == null || !value.contains('@') || value.contains(' ')) {
                                  return 'Please enter a valid email address';
                                }
                                else {
                                  return null;
                                }
                              },
                            ),
                            const SizedBox(height: 15.0,),

                            DefaultTextFormField(
                              controller: _psw,
                              icon: Icons.password,
                              label: "Password",
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return "Please enter the password";
                                }
                                else {
                                  return null;
                                }
                              }
                            ),
                            const SizedBox(height: 10.0,),

                            // Reset Password Button
                            TextButton(
                              onPressed: () async {
                                await showResetPasswordDialog(context);
                              },
                              child: const Text(
                                "Forgot your password?",
                                style: TextStyle(
                                  color: Colors.blueGrey,
                                  fontSize: 12.0,
                                  fontWeight: FontWeight.bold,
                                  fontStyle: FontStyle.italic,
                                ),
                              )
                            ),
                            const SizedBox(height: 15.0,),

                            ElevatedButton(
                              onPressed: () async {
                                setState(() => _errorMessage = '');
                                if (_formKey.currentState!.validate()) {
                                  // If no google message is shown, proceed with email account migration
                                  if (_googleMessage == '') {
                                    String? v1uid = await _firebaseAuth.logIn(_email.text, _psw.text);
                                    _migrator.migrateUser(v1uid, _psw.text);
                                  }
                                  // otherwise it means the user is already logged in with google, so just create an appwrite account for him
                                  else {
                                    _migrator.createAppwriteUser(
                                      email: _email.text,
                                      password: _psw.text,
                                    );
                                  }
                                }
                              },
                              child: const Padding(
                                padding: EdgeInsets.symmetric(vertical: 11.0,),
                                child: Text(
                                  "MIGRATE",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              )
                            ),

                            if(_migrationProgress > 0)
                              const SizedBox(height: 25.0,),
                            if(_migrationProgress > 0)
                              Stack(
                                children: [
                                  CircularProgressIndicator(
                                    value: _migrationProgress/100,
                                  ),
                                  if (_migrationProgress == 100)
                                    const Padding(
                                      padding: EdgeInsets.only(top: 5.0, left: 5.0),
                                      child: Icon(Icons.check, color: Colors.blueGrey, size: 28.0,),
                                    ),
                                ],
                              ),

                            if (_errorMessage != '' || _successMessage != '')
                              const SizedBox(height: 25.0,),

                            if (_errorMessage != '')
                              Text(
                                _errorMessage,
                                style: TextStyle(
                                  color: Colors.red[700],
                                  fontStyle: FontStyle.italic,
                                ),
                                textAlign: TextAlign.center,
                              ),

                            if (_successMessage != '')
                              Text(
                                _successMessage,
                                style: TextStyle(
                                  color: Colors.green[900],
                                  fontStyle: FontStyle.italic,
                                ),
                                textAlign: TextAlign.center,
                              ),
                          ],
                        )
                      )
                      //endregion
                    ],
                  ),
                ),
              ),
            ),
          ),
          // Padding
          Flexible(
              flex: 1,
              child: Container()
          ),
        ],
      ),
    );
  }
}
