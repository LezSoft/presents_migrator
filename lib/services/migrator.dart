/*
 * This file is part of presents_migrator.
 *   presents_migrator is free software developed by LezSoft. While LezSoft holds all rights on the presents_migrator brand and logo,
 *   you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 *   Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 *   SPDX-FileCopyrightText:  © 2020-2022 Lezsoft <https://lezsoft.com>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'dart:async';

import 'package:presents_migrator/models/v1_user.dart';
import 'package:presents_migrator/services/appwrite.dart';
import 'package:presents_migrator/services/firestore_db.dart';

class Migrator {

  late FirestoreDBService _firestoreDB;
  final AppwriteService _aw = AppwriteService();

  V1User? _v1User;

  final StreamController<double> _migrationProgress = StreamController<double>();
  Stream<double> get migrationProgress => _migrationProgress.stream.asBroadcastStream();

  /// Migrate the user with the given UID
  Future<void> migrateUser(String? v1uid, String? password, {bool fromGoogleLogin = false}) async {

    // start the migration progress stream (5%)
    _migrationProgress.sink.add(5);

    // return an error if the uid received is nullfuture
    if (v1uid == null) {
      _migrationProgress.sink.addError("ERROR: PresentsV1 User not found. Please check your credentials");
      return;
    }
    else {
      // get the user with the matching uid from firestore db
      _firestoreDB = FirestoreDBService(v1uid);
      _v1User = await _firestoreDB.matchingUser(v1uid);

      // update migration progress to 20%
      _migrationProgress.sink.add(20);

      // if no user was found return an error
      if (_v1User == null) {
        _migrationProgress.sink.addError("ERROR: PresentsV1 User not found. Please check your credentials");
        return;
      }
      // else if the function has been called after a google login return the google-message
      else if (fromGoogleLogin) {
        _migrationProgress.sink.addError("Successully logged in with google, now please enter the email and password want to use in the future to login in Presents V2");
      }
      // else (the function was called after an email login and _v1User is not null)...
      else {
        // proceed with the creation of the new user
        await createAppwriteUser(
          email: _v1User!.email!,
          password: password!
        );
      }
    }
  }

  Future<void> createAppwriteUser({required String email, required String password}) async {
    // Get the current _v1User
    late V1User v1user;
    if (_v1User != null) {
      v1user = _v1User!;
      v1user.email = email;
    }
    else {
      _migrationProgress.sink.addError("Something went wrong during the migration, please try again.");
    }

    // Ensure that the migrator won't create duplicates
    String? existanceCheck = await _aw.checkIfUserExists(email: _v1User!.email!, username: _v1User!.username!);

    // update migration progress to 40%
    _migrationProgress.sink.add(40);

    if (existanceCheck != null) {
      // if an user with the same email has been found, return an error
      if (existanceCheck.contains('email')) {
        _migrationProgress.sink.addError("ERROR: You've already migrated your account to Presents V2");
        return;
      }
      // else if an user with the same username (but different email) has been found, randomize the username (and proceed)
      else if (existanceCheck.contains('username')) {
        _v1User!.username = "${_v1User!.username}_${DateTime
            .now()
            .second}";
      }
    }

    // Get from Firestore the list of Presents as a list of json
    List<Map<String, dynamic>> v1Presents = await _firestoreDB.getPresentsAsListOfJSON(v1user.uid);

    // update migration progress to 60%
    _migrationProgress.sink.add(60);

    // Create a new user on Appwrite with the data fetched from Firebase
    String? res = await _aw.createNewUser(
        email: v1user.email!,
        username: v1user.username!,
        password: password,
        name: v1user.name,
        birthday: v1user.birthday?.toString(),
        about: v1user.about,
    );

    // If there has been an error during user creation return it and stop
    if (res != null) {
      _migrationProgress.sink.addError(res);
      return;
    }

    // update migration progress to 80%
    _migrationProgress.sink.add(80);

    res = await _aw.createPresentsColl(v1Presents);

    if (res != null) {
      _migrationProgress.sink.addError(res);
      return;
    }

    // update migration progress to 100%
    _migrationProgress.sink.add(100);
    // close the progress stream
    _migrationProgress.sink.close();
  }
}