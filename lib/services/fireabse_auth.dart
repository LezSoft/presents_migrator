/*
 * This file is part of presents_migrator.
 *   presents_migrator is free software developed by LezSoft. While LezSoft holds all rights on the presents_migrator brand and logo,
 *   you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 *   Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 *   SPDX-FileCopyrightText:  © 2020-2022 Lezsoft <https://lezsoft.com>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

class FirebaseAuthService {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn _googleSignIn = GoogleSignIn(clientId: "592870937461-arldm3lubhvdqltvqf5g4d14pm49kscu.apps.googleusercontent.com");

  // Log In Function
  Future<String?> logIn(String email, String password) async {
    try {
      UserCredential result = await _auth.signInWithEmailAndPassword(
        email: email,
        password: password,
      );
      return result.user!.uid;
    } catch (e) {
      return null;
    }
  }

  Future<String?> googleLogIn() async {
    final GoogleSignInAccount? googleUser = await _googleSignIn.signIn();
    if (googleUser == null) {
      return "process-aborted";
    }
    else {
      final GoogleSignInAuthentication googleAuth = await googleUser.authentication;
      final AuthCredential credential = GoogleAuthProvider.credential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );

      try {
        UserCredential result = await _auth.signInWithCredential(credential);
        return result.user!.uid;
      }
      catch (e) {
        return null;
      }
    }

  }

  // Reset Password Function
  Future<String?> resetPassword(String email) async {
    try {
      await _auth.sendPasswordResetEmail(email: email);
      return null;
    } catch (e) {
      String message = e.toString().split(']').last;
      return message;
    }
  }

  // Log Out Function
  Future logOut() async {
    try {
      return await _auth.signOut();
    } catch (e) {
      return null;
    }
  }
}