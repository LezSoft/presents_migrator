/*
 * This file is part of presents_migrator.
 *   presents_migrator is free software developed by LezSoft. While LezSoft holds all rights on the presents_migrator brand and logo,
 *   you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 *   Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 *   SPDX-FileCopyrightText:  © 2020-2022 Lezsoft <https://lezsoft.com>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'package:appwrite/appwrite.dart';
import 'package:appwrite/models.dart' as awm;

class AppwriteService {
  // Constructor
  AppwriteService() {
    _client = Client();

    _client
        .setEndpoint("https://server.lezsoft.com/v1")
        .setProject("presents");

    _account = Account(_client);
    _db = Databases(_client);
    _functions = Functions(_client);
  }

  late Client _client;
  late Account _account;
  late Databases _db;
  late Functions _functions;
  String? _uid;

  Future<String?> createNewUser({
    required String email,
    required String username,
    required String password,
    String? name,
    String? birthday,
    String? about,
  }) async {
    try {
      // create a new AppWrite User
      awm.Account u = await _account.create(
          userId: 'unique()', name: username, email: email, password: password);
      // create a session for the new user
      await _account.createEmailSession(email: email, password: password);

      // Save created user's uid for later use
      _uid = u.$id;

      // Create the document for this user's data
      await _db.createDocument(
          databaseId: 'default',
          collectionId: 'userData',
          documentId: u.$id,
          data: {
            'username': username,
            'email': email,
            if (name != null) 'name': name,
            if (about != null) 'about': about,
            if (birthday != null) 'birthday': birthday,
          });

      return null;
    } catch (e) {
      return e.toString();
    }
  }

  // A Function that will return 'email' and/or 'username' if there is already a user in the database with this email/username (or null if no user was found)
  Future<String?> checkIfUserExists({required String email, required String username}) async {
    bool foundEmail = false;
    bool foundUsername = false;

    List<awm.Document> usersDataDocs = (await _db.listDocuments(
      databaseId: 'default',
      collectionId: 'userData')).documents;

    for (awm.Document doc in usersDataDocs) {
      if (doc.data['email'] == email) {
        foundEmail = true;
      }
      if (doc.data['username'] == username) {
        foundUsername = true;
      }
      if (foundEmail && foundUsername) {
        break;
      }
    }

    if (foundEmail && foundUsername) {
      return "email username";
    }
    else if (foundEmail) {
      return "email";
    }
    else if (foundUsername) {
      return "username";
    }
    else {
      return null;
    }
  }

  Future<String?> createPresentsColl(List<Map<String, dynamic>> v1Presents) async {
    try {
      await _functions.createExecution(
        functionId: 'wishlist_coll_creator',
        data: _uid,
        xasync: false,
      );

      for (Map<String, dynamic> present in v1Presents) {
        await _db.createDocument(
          databaseId: 'wishlists_db',
          collectionId: _uid!,
          documentId: present['id'],
          data: {
            'title': present['title'],
            'price': present['price'],
            'priority': present['priority'],
            'link': present['link']
          }
        );
      }

      return null;
    }
    catch (e) {
      return e.toString();
    }
  }
}