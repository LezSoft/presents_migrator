/*
 * This file is part of presents_migrator.
 *   presents_migrator is free software developed by LezSoft. While LezSoft holds all rights on the presents_migrator brand and logo,
 *   you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 *   Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 *   SPDX-FileCopyrightText:  © 2020-2022 Lezsoft <https://lezsoft.com>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:presents_migrator/models/v1_user.dart';

class FirestoreDBService {
  FirestoreDBService(this.uid);

  final String uid;

  // get (or create if it doesn't exist) the 'user_data' collection
  final CollectionReference userDataCollection = FirebaseFirestore.instance.collection('user_data');

  // get (or create) the presents collection
  final presentsCollection = FirebaseFirestore.instance.collection('presents');

  // get a V1User model from a document snapshot
  V1User? _userDataFromSnapshot(DocumentSnapshot snapshot) {
    Map<String, dynamic> snapData = snapshot.data() as Map<String, dynamic>;
    return V1User(
      uid: snapData['uid'],
      email: snapData['email'],
      // profilePic: _getProfilePicFromUrl(snapshot.data()['profilePicUrl']),
      name: snapData['name'],
      username: snapData['username'],
      birthday: snapData['birthday']?.toDate(),
      about: snapData['about'],
    );
  }

  // return a MyUserData with the given uid, or null if there is no user with this uid
  Future<V1User?> matchingUser(String uid) async {
    // get a list of all users in the database
    QuerySnapshot usersSnap = await userDataCollection.get();
    List<V1User?> users = usersSnap.docs.map(_userDataFromSnapshot).toList();

    // check if there is a user with the given uid and return it
    for (var i = 0; i < users.length; i++) {
      V1User? user = users[i];
      if (user != null && user.uid == uid) {
        return user;
      }
    }

    // if nothing has been returned there is no matching user, so return null
    return null;
  }

  Future<List<Map<String, dynamic>>> getPresentsAsListOfJSON(String uid) async {
    List<Map<String, dynamic>> presentsList = List<Map<String, dynamic>>.empty(growable: true);

    QuerySnapshot<Map<String, dynamic>> presentsSnap = await presentsCollection.doc(uid).collection('wishes').get();
    List<DocumentSnapshot> docSnaps = presentsSnap.docs;
    for (DocumentSnapshot doc in docSnaps) {
      Map<String, dynamic> v1Present = doc.data() as Map<String, dynamic>;
      Map<String, dynamic> v2Present = <String, dynamic>{
        'id': "imported_${v1Present['presentID'].replaceAll(' ', '_').replaceAll(':', '-')}",
        'title': v1Present['title'],
        'price': double.parse(v1Present['price']),
        'priority': v1Present['wishLevel'],
        'link': v1Present['link'],
        'donorUid': null,
      };
      presentsList.add(v2Present);
    }

    return presentsList;
  }
}