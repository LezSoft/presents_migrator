/*
 * This file is part of presents_migrator.
 *   presents_migrator is free software developed by LezSoft. While LezSoft holds all rights on the presents_migrator brand and logo,
 *   you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 *   Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 *   SPDX-FileCopyrightText:  © 2020-2022 Lezsoft <https://lezsoft.com>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'package:flutter/material.dart';

/// Customized TextFormField to be used as default
class DefaultTextFormField extends StatefulWidget {
  //Constructor
  DefaultTextFormField({
    Key? key,
    required this.controller,
    required this.icon,
    required this.label,
    required this.validator,
    this.keyboardType,
  }) : super(key: key) {
    isPassword = label.contains("Password");
  }

  final TextEditingController controller;
  final IconData icon;
  final String label;
  final String? Function(String?) validator;
  final TextInputType? keyboardType;
  late final bool isPassword;

  @override
  State<DefaultTextFormField> createState() => _DefaultTextFormFieldState();
}

class _DefaultTextFormFieldState extends State<DefaultTextFormField> {

  bool _showPassword = false;

  @override
  Widget build(BuildContext context) {

    return Material(
      elevation: 8.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: TextFormField(
        controller: widget.controller,
        validator: widget.validator,
        obscureText: widget.isPassword && !_showPassword,
        decoration: InputDecoration(
          prefixIcon: Icon(widget.icon),
          labelText: widget.label,
          suffixIcon: widget.isPassword ? IconButton(
            icon: Icon(
              _showPassword ? Icons.visibility : Icons.visibility_off,
            ),
            padding: const EdgeInsets.only(right: 8.0),
            onPressed: () {
              setState(() => _showPassword = !_showPassword);
            },
          ) : null,
        ),
        keyboardType: widget.keyboardType,
      ),
    );
  }
}