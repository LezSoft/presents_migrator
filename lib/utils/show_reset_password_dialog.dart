/*
 * This file is part of presents_migrator.
 *   presents_migrator is free software developed by LezSoft. While LezSoft holds all rights on the presents_migrator brand and logo,
 *   you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 *   Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 *   SPDX-FileCopyrightText:  © 2020-2022 Lezsoft <https://lezsoft.com>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'package:flutter/material.dart';
import 'package:presents_migrator/services/fireabse_auth.dart';
import 'package:presents_migrator/utils/default_text_form_field.dart';

Future<void> showResetPasswordDialog(BuildContext context) async {

  TextEditingController controller = TextEditingController();

  String? email = await showDialog(
    context: context,
    barrierDismissible: false,
    builder: (context) => AlertDialog(
      title: const Text('Reset Presents V1 Password'),
      content: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            const Text("Enter your email, if there is a V1 account associated with it you will receive an email to reset your password"),
            const SizedBox(height: 20.0,),
            DefaultTextFormField(
                controller: controller,
                icon: Icons.alternate_email,
                label: 'Email',
                validator: (value) {
                  if (value == null || !value.contains('@') || !value.contains('.')) {
                    return "Please enter a valid email address";
                  }
                  else {
                    return null;
                  }
                }
            ),
          ]),
      // backgroundColor: Colors.orange[50],
      actions: <Widget>[
        TextButton(
          child: const Text('Cancel', style: TextStyle(
            // color: Colors.grey[850],
            fontWeight: FontWeight.bold,
            fontSize: 18.0,
          )),
          onPressed: () {
            Navigator.of(context).pop(null);
          },
        ),
        TextButton(
          child: const Text('Send', style: TextStyle(
            // color: Colors.grey[850],
            fontWeight: FontWeight.bold,
            fontSize: 18.0,
          )),
          onPressed: () {
            Navigator.of(context).pop(controller.text);
          },
        ),

      ],
    ),
  );

  String? error;
  if (email == null) {
    return null;
  }
  error = await FirebaseAuthService().resetPassword(email);

  WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
    ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Row(
              children: <Widget>[
                Icon((error == null) ? Icons.thumb_up : Icons.error, color: Colors.white,),
                const SizedBox(width: 20.0,),
                Text((error == null) ? 'Email sent, check your inbox!' : 'Something went wrong:\n$error'),
              ]),
          backgroundColor: (error == null) ? Colors.green.withOpacity(0.75) : Colors.red.withOpacity(0.75),
        )
    );
  });
}