/*
 * This file is part of presents_migrator.
 *   presents_migrator is free software developed by LezSoft. While LezSoft holds all rights on the presents_migrator brand and logo,
 *   you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 *   Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 *   SPDX-FileCopyrightText:  © 2020-2022 Lezsoft <https://lezsoft.com>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'package:cloud_firestore/cloud_firestore.dart';

class V1User {
  V1User({
    required this.uid,
    this.email,
    this.profilePic,
    this.name,
    this.username,
    this.birthday,
    this.about,
  });

  final String uid;
  String? email;
  dynamic profilePic;
  String? name;
  String? username;
  DateTime? birthday;
  String? about;
}