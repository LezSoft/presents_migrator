<!--
  This file is part of presents_migrator.
  presents_migrator is free software developed by LezSoft. While LezSoft holds all rights on the presents_migrator brand and logo,
  you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
  Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
  
  SPDX-FileCopyrightText:  © 2020-2022 Lezsoft <https://lezsoft.com>
  SPDX-License-Identifier: GPL-3.0-or-later
-->

# presents_migrator

A simple Web Application to migrate your PresentsV1 account to PresentsV2

### Additional Info

This project has been developed to migrate [Presents](https://codeberg.org/lezsoft/Presents) account and data from V1 that used Firebase as a backend) to V2 (that is using Appwrite instead).
Even though some parts of the code are specifically build around Presents, feel free to fork this project and tweak it to migrate your app data from Firebase to Appwrite.

### Copyright and License
This project has been developed by [LezSoft](https://lezsoft.com).
All the rights on the *presents_migrator* brand (with its name and logo) are reserved.

*presents_migrator* is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

Presents is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the [GNU General Public License](LICENSES/GPL-3.0-or-later.txt) for more details.